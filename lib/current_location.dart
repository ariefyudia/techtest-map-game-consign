import 'dart:async';
import 'dart:convert' as convert;
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CurrentLocationPage extends StatefulWidget {
  const CurrentLocationPage({super.key});

  @override
  State<CurrentLocationPage> createState() => _CurrentLocationPageState();
}

class _CurrentLocationPageState extends State<CurrentLocationPage> {
  GoogleMapController? mapController; //contrller for Google map
  TextEditingController keywordController = TextEditingController();
  Set<Marker> markers = Set(); //markers for google map
  LatLng showLocation = LatLng(27.7089427, 85.3086209);
  bool isSearch = false;
  bool isMock = false;
  String typeLocation = "restaurant";

  Future<Position> getUserCurrentLocation() async {
    await Geolocator.requestPermission()
        .then((value) {})
        .onError((error, stackTrace) async {
      await Geolocator.requestPermission();
      print("ERROR" + error.toString());
    });

    return await Geolocator.getCurrentPosition();
  }

  Future getNearby(keyword, type, lat, lng) async {
    markers.clear();
    var url =
        Uri.https('maps.googleapis.com', '/maps/api/place/nearbysearch/json', {
      'keyword': keyword,
      'location': '${lat},${lng}',
      'radius': '10000',
      'types': type,
      'key': 'AIzaSyDbyXEfhnqzftLTfQ4zfrMuZ5r1OV8T0lE'
    });

    var response = await http.get(url);
    // log(url.toString());
    // log(response.body);
    var jsonResponse =
        convert.jsonDecode(response.body) as Map<String, dynamic>;
    // print(jsonResponse['results']);
    for (var element in jsonResponse['results']) {
      Uint8List bytes = (await NetworkAssetBundle(Uri.parse(element['icon']))
              .load(element['icon']))
          .buffer
          .asUint8List();

      markers.removeAll({markers});
      markers.add(Marker(
        markerId: MarkerId("1"),
        position: LatLng(lat, lng),
        infoWindow: InfoWindow(
          title: 'My Current Location',
        ),
        draggable: true,
        onDragEnd: (value) async {
          print({
            'new position ',
            value.latitude.toString(),
            value.longitude.toString()
          });
          setState(() {
            showLocation = LatLng(value.latitude, value.longitude);
          });
          await getNearby(
              keyword, typeLocation, value.latitude, value.longitude);
        },
      ));
      markers.add(Marker(
          markerId: MarkerId(element.toString()),
          position: LatLng(element['geometry']['location']['lat'],
              element['geometry']['location']['lng']),
          infoWindow:
              InfoWindow(title: element['name'], snippet: element['vicinity']),
          icon: BitmapDescriptor.fromBytes(bytes)));
    }
    CameraPosition cameraPosition = new CameraPosition(
      target: LatLng(lat, lng),
      zoom: 14,
    );

    // final GoogleMapController controller = await _controller.future;
    mapController!
        .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
    setState(() {
      showLocation = LatLng(lat, lng);
    });
  }

  @override
  void initState() {
    getUserCurrentLocation().then((value) async {
      if (value.isMocked) {
        setState(() {
          isMock = true;
        });
      } else {
        setState(() {
          isMock = false;
        });
      }
      // marker added for current users location
      markers.add(Marker(
        markerId: MarkerId("1"),
        position: LatLng(value.latitude, value.longitude),
        infoWindow: InfoWindow(
          title: 'My Current Location',
        ),
        draggable: true,
        onDragEnd: (value) async {
          setState(() {
            showLocation = LatLng(value.latitude, value.longitude);
          });
          await getNearby('', typeLocation, value.latitude, value.longitude);
        },
      ));

      // specified current users location
      CameraPosition cameraPosition = new CameraPosition(
        target: LatLng(value.latitude, value.longitude),
        zoom: 20,
      );

      // final GoogleMapController controller = await _controller.future;
      mapController!
          .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
      setState(() {
        showLocation = LatLng(value.latitude, value.longitude);
      });

      await Future.delayed(Duration(milliseconds: 10));
      await getNearby('', typeLocation, value.latitude, value.longitude);
    });

    //you can add more markers here
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: !isSearch
            ? Text("Google Map in Flutter")
            : TextFormField(
                onFieldSubmitted: (value) {
                  getNearby(keywordController.text, typeLocation,
                      showLocation.latitude, showLocation.longitude);
                },
                controller: keywordController,
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                    hintText: 'Search ...',
                    fillColor: Colors.white,
                    focusColor: Colors.white),
              ),
        backgroundColor: Colors.deepPurpleAccent,
        actions: [
          // Navigate to the Search Screen
          IconButton(
              onPressed: () {
                setState(() {
                  if (isSearch) {
                    isSearch = false;
                  } else {
                    isSearch = true;
                  }
                });
              },
              // => Navigator.of(context)
              //     .push(MaterialPageRoute(builder: (_) => const SearchPage())),
              icon: !isSearch ? Icon(Icons.search) : Icon(Icons.close))
        ],
      ),
      body: Stack(children: [
        GoogleMap(
          //Map widget from google_maps_flutter package
          zoomGesturesEnabled: true, //enable Zoom in, out on map
          initialCameraPosition: CameraPosition(
            //innital position in map
            target: showLocation, //initial position
            zoom: 14.0, //initial zoom level
          ),
          markers: markers, //markers to show on map
          mapType: MapType.normal, //map type
          onMapCreated: (controller) {
            //method called when map is created
            setState(() {
              mapController = controller;
            });
          },
        ),
        Positioned(
            //widget to display location name
            top: isMock ? 20 : 0,
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: EdgeInsets.all(8),
              child: Card(
                child: Container(
                  padding: EdgeInsets.all(0),
                  width: MediaQuery.of(context).size.width - 40,
                  child: Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Expanded(
                          flex: 5,
                          child: RadioListTile(
                            title: Text("Restaurant"),
                            value: "restaurant",
                            groupValue: typeLocation,
                            onChanged: (value) {
                              setState(() {
                                typeLocation = value.toString();
                              });
                              getNearby("", typeLocation, showLocation.latitude,
                                  showLocation.longitude);
                            },
                          )),
                      Expanded(
                          flex: 5,
                          child: RadioListTile(
                            title: Text("Hospital"),
                            value: "hospital",
                            groupValue: typeLocation,
                            onChanged: (value) {
                              setState(() {
                                typeLocation = value.toString();
                              });
                              getNearby("", typeLocation, showLocation.latitude,
                                  showLocation.longitude);
                            },
                          )),
                    ],
                  ),
                ),
              ),
            )),
        if (isMock)
          Positioned(
              //widget to display location name
              // top: 5,
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Card(
                  color: Colors.red,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Container(
                        padding: EdgeInsets.all(0),
                        width: MediaQuery.of(context).size.width - 40,
                        child: Text(
                          'Your location is indicated to be fake',
                          style: TextStyle(color: Colors.white),
                        )),
                  ),
                ),
              ))
      ]),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Colors.green.shade400,
        foregroundColor: Colors.black,
        onPressed: () async {
          log(showLocation.latitude.toString());
          SharedPreferences pref = await SharedPreferences.getInstance();
          pref.setString("lat", "${showLocation.latitude.toString()}");
          pref.setString("lng", "${showLocation.longitude.toString()}");
        },
        icon: Icon(Icons.add),
        label: Text('Save Location'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
    );
  }
}
